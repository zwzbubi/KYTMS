package com.kytms.feetype.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 费用类型dao
 * @author 臧英明
 * @create 2018-01-04
 */
public interface FeeTypeDao<FeeType> extends BaseDao<FeeType> {
}
